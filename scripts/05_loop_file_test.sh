#!/usr/bin/env bash

source 04_func_file_test.sh

declare -a arr=($1 $2)  # declare variable arr as array

for each in "${arr[@]}"
do
	func_file_test $each
done
