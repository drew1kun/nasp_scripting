#!/usr/bin/env bash

files=`ls /etc/sysconfig/network-scripts/`

mkdir '/tmp/test/'

for i in ${files}
do
	cp "/etc/sysconfig/network-scripts/${i}" "/tmp/test/${i}.bak"
done

