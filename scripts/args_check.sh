# Check for arguments.
#
# Call function example:
# args_check ${args_number} ${args_needed} "${usage_example}"
#

args_check () {
	args_number=$1
	args_needed=$2
	example=$3
	if [[ ${args_number} < ${args_needed} ]]; then
		echo 'Please specify arguments.'
		echo 'The script usage:'
		echo ${example}
		exit 1
	fi
}
