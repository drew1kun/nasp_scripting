#!/usr/bin/env bash


# Task 1
# Emulate grep with sed. Print all lines starting with option:
sed -n '/option/p' ./dhcp


# Task 2
# Delete all blank lines in the file (and write the result in the file dhcp.result):
cat ./dhcp | sed '/^$/d' > ./dhcp.result


# Task 3
# Add the following line to the beginning of the file: # NASP19 sed DHCP Configuration
sed -i '1i# NASP19 sed DHCP Configuration' ./dhcp.result


# Task 4
# Remove all comments at the end of line
sed -i '/^#/! s/#.*$//' ./dhcp.result


# Task 5
# Change the router IP address to 10.10.0.1. Note that there is more than one empty space between "option" and "routers"
sed -i -r '/^[[:space:]]*option[[:space:]]*routers[[:space:]].*$/s/((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])/10.10.0.1/' ./dhcp.result


# Task 6
# Report the range of IP addresses. The output should be in the form: IP range is : 10-100. I.e., do not report the rest of IP addresses.
sed -n -r 's/[[:space:]]range[[:space:]]((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])[[:space:]]((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5]).*$/The dhcp range is: \3-\6/p' dhcp.result
# Here we substitute the whole matching pattern with just some elements of that same pattern.


# Task 7
# Prompt the user to change the address range (last octet in the /24 network).
# Your script shall accept a range, check the range for validity, and update the file.
# Do not assume that you know the network part of the range.
# Only the range itself should be updated by sed.
echo 'please specify the start of ip range (host portion only). For example: 10'

while read line
do
	line1=$line
	break
done < "${1:-/dev/stdin}"

echo 'please specify the end of ip range (host portion only). For example: 100'

while read line
do
	line2=$line
	break
done < "${1:-/dev/stdin}"

if [ ${line1} -lt ${line2} -a ${line1} -lt 255 -a ${line2} -lt 255 ]; then
	sed -i -r \
	"/[[:space:]]range[[:space:]]/ s/(((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3})(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])([[:space:]])(((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3})(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5]).*$/\1${line1}\5\6${line2}/" \
	dhcp.result
else
	echo "the range ${line1}-${line2} is wrong"
fi

