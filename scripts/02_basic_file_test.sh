#!/usr/bin/env bash
declare file_name
declare usage_example='basic_file_test.sh file.txt'

source args_check.sh
args_check $# 1 "${usage_example}"

#if [[ $# < 1 ]]; then
#	echo "Please specify the file name as an argument"
#	echo "For example: 'basic_file_test.sh file.txt'"
#	exit 1
#fi

file_name=$1

if [[ -f $file_name ]]; then
	echo "The filename: $file_name exists"
	exit 0
else
	echo "The filename: $file_name does not exist"
	exit 1
fi
