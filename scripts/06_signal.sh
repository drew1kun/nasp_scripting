#!/usr/bin/env bash
declare usage_example
declare -a signals=( 1 2 10 15 )
declare pid=$1
declare signal=$2

usage_example='06_signal.sh <PID> <Signal_ID>'

source args_check.sh

echo ${usage_example}
args_check $# 2 "${usage_example}"

check_sig() {
	exists=1

	for i in ${signals[@]}
	do 
		if [[ ${signal} == $i ]]
			then exists=0
		fi
	done

	if [[ ${exists} = 1 ]]; then
		echo "The signal ID is not supported"
		echo "Please specify the valid one: 1, 2, 15 or 10"
		exit 1
	fi
}


#MAIN
args_check $# 2 ${example}
check_sig ${signal}
kill -s ${signal} ${pid}

