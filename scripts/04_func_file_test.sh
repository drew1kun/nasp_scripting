#!/usr/bin/env bash

func_file_test() {
	declare file_name

	if [[ -z "$1" ]]; then
		echo "Please specify the file name as an argument"
		echo "For example: 'basic_file_test.sh file.txt'"
		exit 1
	fi

	file_name=$1

	if [[ -f $file_name ]]; then
		echo "The file: $file_name exists"
		#exit 0
		return 0
	else
		echo "The file: $file_name does not exist"
		#exit 1
		return 1
	fi
}
