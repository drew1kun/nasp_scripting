# README #
This is a student repository for Linux Scripting Class (BCIT NASP Certificate Program).


### Repo Content ###

* The [scripts](https://bitbucket.org/drew-kun/nasp_scripting/src/99b798e6eb27546447dc763b183bc8ce9a2991f2/scripts/?at=master) folder contains the basic scripting lessons activity submissions.

* The [project](https://bitbucket.org/drew-kun/nasp_scripting/src/31db8ce9fbed8ac56ce0fb934822309eae5f6520/project/?at=master) folder contains the Final Project submission for this course.


### Installation Guidelines ###
* Please [read](https://bitbucket.org/drew-kun/nasp_scripting/src/509a58f36f06e8122af67dc59dd481a83e6163f9/project/README.md?at=master&fileviewer=file-view-default) before running **project script**

### Contacts ###

* If you see any issues with this repository, please contact me at:

 *drewshg@gmail.com*