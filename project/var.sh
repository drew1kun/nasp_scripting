#!/usr/bin/env bash

source config.sh
source functions.sh

declare mask_wan=$(cidr2mask "${cidr_wan}")
declare mask_lan=$(cidr2mask "${cidr_lan}")
declare mask_wlan=$(cidr2mask "${cidr_wlan}")

# Specify network IPs:
declare -A net=(
    ['WAN']="${net_wan}"
    ['LAN']="${net_lan}"
    ['WLAN']="${net_wlan}"
)
# Specify subnet masks:
declare -A mask=(
    ['WAN']="${mask_wan}"
    ['LAN']="${mask_lan}"
    ['WLAN']="${mask_wlan}"
)
# SPECIFY interface ip addresses:
declare -A ip=(
    ['WAN']="${ip_wan}"
    ['LAN']="${ip_lan}"
    ['WLAN']="${ip_wlan}"
)

# interface names:
declare -A if=(
    ['WAN']="${if_wan}"
    ['LAN']="${if_lan}"
    ['WLAN']="${if_wlan}"
)

#interface cidr bits:
declare -A cidr=(
    ['WAN']="${cidr_wan}"
    ['LAN']="${cidr_lan}"
    ['WLAN']="${cidr_wlan}"
)

#------------------DNS-------------------
declare ptr_name=$(ptr "${cidr[LAN]}" "${net[LAN]}")

declare -A dns_zone=(
    ['DOMAIN']="${domain}"
    ['PTR_LAN']="${ptr_name}"
)
#---------------------------------------

#---------DHCP Configuration------------
# DHCP Ranges for all subnets:
declare -A dhcp_range_START=(
    ['LAN']="${dhcp_range_start_lan}"
    ['WLAN']="${dhcp_range_start_wlan}"
)

declare -A dhcp_range_END=(
    ['LAN']="${dhcp_range_end_lan}"
    ['WLAN']="${dhcp_range_end_wlan}"
)

# DHCP Reservations:
declare -A dhcp_resrv=( ['MAC_ADDRESS_1']="${srv[MAIL]}" )

declare -a arrays=( 'net' 'mask' 'ip' 'if' 'cidr' 'dhcp_range_START' 'dhcp_range_END'  )
if [[ $wireless -eq 0 ]]; then
    for i in ${arrays[@]}; do
        unset "${i}[WLAN]"
        #declare -p ${i}
    done
fi
#---------------------------------------

#----------SET LOGGING FORMAT-----------
declare NOW=$(date +"%b-%d-%y-%H%M%S")
declare LOGFILE="/tmp/install-${NOW}.log"
#---------------------------------------

function final_msg() {

    ip_addr=$1

    msg="\nATTENTION!
Please poweroff the system and make necessary Network Settings modifications for your VM.
For example:
Insert a wifi usb dongle and filter it to a vim (if setting up router with wireless AP)
OR
bridge NICs to a correct Layer 2 networks etc.

If you are currently using ssh connection, then this session will be interrupted.
Please reconnect to this machine using ssh. New ip address is ${ip_addr} as specified in config.sh\n\n"

printf "${msg}"
}

