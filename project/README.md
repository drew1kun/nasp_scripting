#README#


## CentOS7 Router/Mail Server automated setup script##


This script will set up fresh installed CentOS7 box to act in one of two modes:

**router** (with or without wireless interface, which acts as an Access Point) or **mailserver**

For Router setup we need a centos 7 host with 2 NIC adapters
(preferably Paravirtualized Network virtion-net)

For mailserver setup one adapter will be enough


###IMPORTANT NOTES###

Use CentOS 7.
Check and modify the 'config.sh' file before running 'install.sh'

The script changes the network configuration in accordance with the content of 'config.sh'.
If you run the script using ssh access then you will need to restart the network manually
using 'systemctl restart network.service'.
This will interrupt current ssh session and you will need to re-establish ssh connection using
new ip address or dns name.


###BASIC CONFIGURATION (config.sh)###

if building wireless router, set

```
#!bash

wireless=1
```

otherwise set it to 0

for setting up dns (nsd as Authoritative Server and unbound as Caching Server), do:

```
#!bash

dns=1
```


To run dhcp server use:

```
#!bash

dhcp=1
```


To setup ospf dynamic routing use:

```
#!bash

routing=1
```


To run default firewall configuration:

```
#!bash

firewall=1
```


You can also add your own firewall rules as iptables commands by modifying 'services/iptables.sh'


###INSTALLATION USING VAGRANT CENTOS 7 BASE BOX###

Install latest version of [Vagrant](https://www.vagrantup.com/)

Get the CentOS7 basebox from [atlas](https://atlas.hashicorp.com/boxes/search)

Spin up a vagrant centos7-minimal vm:


```
#!bash

mkdir ~/vagrant/centos7
cd ~/vagrant/centos7
vagrant init centos/7
```


Then modify the Vagrantfile, apply the necessary provision.

```
#!bash

vim ~/vagrant/centos7/Vagrantfile
```

**Note**: The only required provision step is to install git.
You can achieve this by adding next to a Vagrant file:


```
#!bash

$script = <<SCRIPT
    yum update
    yum install -y git
SCRIPT

Vagrant.configure("2") do |config|
    config.vm.provision "shell", inline: $script
end
```


At this stage you can add any post installation shell commands you want. All commands will be run as root during **vagrant up** procedure.

then run:

```
#!bash


vagrant up
```


This should run provision. If there are any troubles with the base box, use another one.
Alternatively, to force box provision, run:


```
#!bash

vagrant provision
```

Ssh into vm and clone the repository:


```
#!bash

vagrant ssh
git clone https://drew-kun@bitbucket.org/drew-kun/nasp_scripting.git
cd nasp_scripting/project
```


Script should be run as root

```
#!bash

sudo -s
```


For settin up router:

```
#!bash

./install.sh router
```


To set up a mailserver:

```
#!bash

./install.sh mail
```


###AUTOMATIC SETUP WITH VAGRANT PROVISION###

It is recommended to add all manual steps as a part of vagrant box provision.
For example:


```
#!bash

$script = <<SCRIPT
    yum update
    yum install -y git
    git clone https://drew-kun@bitbucket.org/drew-kun/nasp_scripting.git
    cd nasp_scripting/project
    ./install.sh router
SCRIPT

Vagrant.configure("2") do |config|
    config.vm.provision "shell", inline: $script
end

```


###FINISH SETUP###
After successfully running the script stop the vm:


```
#!bash

vagrant halt
```


or just 
```
#!bash

poweroff
```
(inside the vm)

modify the Network settings of the vm in Virtuabox (using either GUI or VBoxManage cli):

Add the wifi iterface filter to the VM (If setting up wireless AP).

*For example:*

```
#!bash
VBoxManage modifyvm router —usb on
VBoxManage usbfilter add 0 \
	—-target $name \
	—-name usbstick \
	—-vendorid 0x0b05 \
	—-productid 0x17ab
```

To find out VendorID and ProductID of the USB device, use:
```
#!bash
lsusb
...
Bus 001 Device 004: ID 0b05:17ab ASUSTek Computer, Inc. USB-N13 802.11n Network Adapter (rev. B1) [Realtek RTL8192CU]
...

```

Add additional interfaces if needed and connect to corresponding Layer 2 networks.

*For example:*

Bridge the NIC1 to VLAN2016 and connect NIC2 to an internal network called 'as_net':

```
#!bash

VBoxManage modifyvm router
    --nic1 bridged \
    --bridgeadapter1 VLAN2016 \
    --nicpromisc1 allow-all \
    --nictype1 virtio

VBoxManage modifyvm router \
    --nic2 intnet \
    --intnet1 as_net \
    --nicpromisc2 allow-all \
    --nictype2 virtio
```

Finally start the VM

ENJOY!

