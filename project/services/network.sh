#!/usr/bin/env bash

print_status "SETTING UP THE NETWORK"

print_status "Enabling IPv4 Forwarding"
search_add '^[[:space:]]*net.ipv4.ip_forward=[0-1]' 'net.ipv4.ip_forward=1' '/etc/sysctl.conf' 2>> ${LOGFILE} 1> /dev/null
check_exit "IPv4 Forwarding enabled" "Failed to enable IPv4 Forwarding"

print_status "Configuring network interfaces"
check_dir 'src/etc/sysconfig/network-scripts' 1 2>> ${LOGFILE} 1> /dev/null
check_exit "Network Interfaces files created" "Network interfaces creation incomplete"

declare -A ifcfg_config=()
declare -A ifcfg_string=()

ifcfg_string+=( ['DNS_SERVER']="${dns_forwarder}" )

if [[ ${wireless} -eq 0 ]]; then
    rm -rf /etc/sysconfig/network-scripts/ifcfg-WLAN
    unset "if[WLAN]"
else
    ifcfg_string+=( ['ESSID_WLAN']="${essid_wlan}" )
fi

if [[ ${mailserver} -eq 1 ]]; then
    rm -rf /etc/sysconfig/network-scripts/ifcfg-LAN
    unset "if[LAN]"
fi

print_status "Renaming interface files"

for K in "${!if[@]}"; do
    mv_file "/etc/sysconfig/network-scripts/ifcfg-${K}" "/etc/sysconfig/network-scripts/ifcfg-${if[${K}]}" 1
    ifcfg_config+=( ["${K}"]="/etc/sysconfig/network-scripts/ifcfg-${if[${K}]}" )
    ifcfg_string+=( ["IF_${K}"]="${if[${K}]}" )
    ifcfg_string+=( ["IP_${K}"]="${ip[${K}]}" )
    ifcfg_string+=( ["CIDR_${K}"]="${cidr[${K}]}" )
done

if [[ ${mailserver} -eq 1 ]]; then
    ifcfg_string+=( ['GATEWAY_WAN']="${ip['LAN']}" )
    unset "ifcfg_string[IP_WAN]"
    ifcfg_string+=( ['IP_WAN']="${srv['mail']}" )
else
    ifcfg_string+=( ['GATEWAY_WAN']="${gateway_wan}" )
fi

sed_conf "$(declare -p ifcfg_string)" "$(declare -p ifcfg_config)" "${LOGFILE}"
