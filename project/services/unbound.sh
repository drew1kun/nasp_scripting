#!/usr/bin/env bash

#-------------------UNBOUND-----------------------
print_status "SETTING UP CACHING DNS SERVER"
print_status "Installing 'unbound' (Chaching DNS) package"
yum install -y unbound 2>> ${LOGFILE} 1> /dev/null
check_exit "unbound is installed" "Failed to install unbound"

print_status "Starting unbound service first time"
systemctl start unbound.service 2>> ${LOGFILE} 1> /dev/null
check_exit "unbound service started" "unbound service failed"

print_status "Enabling unbound service to start on boot"
systemctl enable unbound.service 2>> ${LOGFILE} 1> /dev/null
check_exit "unbound service enabled" "unbound service disabled"

print_status "Configuring unbound"
check_dir 'src/etc/unbound' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "/etc/unbound directory copied" "/etc/unbound directory was not copied completely"

print_status "Downloading RootHints"
wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints 2>> ${LOGFILE} 1> /dev/null
check_exit "/etc/unboun/root.hints file was created" "failed to download /etc/unbound/root.hints file"

declare -A unbound_config=(
    ['CONFIG']="/etc/unbound/unbound.conf"
)

declare -A unbound_string=(
    ['DOMAIN']="${domain}"
    ['PTR_LAN']="${ptr_name}"
    ['IP_UNBOUND']="${ip_unbound}"
    ['IP_NSD']="${ip_nsd}"
    ['NET_LAN']="${net[LAN]}"
    ['CIDR_LAN']="${cidr[LAN]}"
    ['DNS_FORWARDER']="${dns_forwarder}"
)
if [[ ${wireless} -eq 0 ]]; then
    sed -i '/WLAN/d' "${unbound_config['CONFIG']}"
else
    unbound_string+=( ['NET_WLAN']="${net[WLAN]}" ['CIDR_WLAN']="${cidr[WLAN]}" )
fi

sed_conf "$(declare -p unbound_string)" "$(declare -p unbound_config)" "${LOGFILE}"

print_status "Checking nsd configuration files for syntax errors"
unbound-checkconf "${unbound_config['CONFIG']}" 2>> ${LOGFILE} 1> /dev/null
check_exit "There are no errors in ${unbound_config['CONFIG']}" "There are some errors in ${unbound_config['CONFIG']}!"
