#!/usr/bin/env bash

print_status "SETTING UP THE DHCP SERVER"

print_status "Installing dhcpd package"
yum install -y dhcp 2>> ${LOGFILE} 1> /dev/null
check_exit "dhcp package installed" "Failed installing dhcp package"

print_status "Enabling DHCP service to start on boot"
systemctl enable dhcpd.service 2>> ${LOGFILE} 1> /dev/null
check_exit "DHCP service enabled" "DHCP service disabled"

print_status "Starting DHCP service first time"
systemctl enable dhcpd.service 2>> ${LOGFILE} 1> /dev/null
check_exit "DHCP service started successfully" "DHCP service failed"

print_status "Configuring DHCP"
check_dir 'src/etc/dhcp' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "DHCP service configured" "DHCP service configuration is incomplete"

declare -A dhcpd_config=(
    ['CONFIG']="/etc/dhcp/dhcpd.conf"
)

declare -A dhcpd_string=(
    ['DOMAIN']="${domain}"
    ['IP_DNS']="${ip_unbound}"
    ['IP_LAN']="${ip[LAN]}"
    ['NET_LAN']="${net[LAN]}"
    ['MASK_LAN']="${mask[LAN]}"
)

# DHCP RESERVATIONS:
if [ ${#ip_reserv_lan[@]} -gt 0 -a ${#mac_reserv_lan[@]} -gt 0 ]; then
    for K in "${!ip_reserv_lan[@]}"; do
    let "n++"
        cat >> /etc/dhcp/dhcpd.conf << EOF
    # LAN RESERVATION_${n}:
    host ${K} {
        hardware ethernet ${mac_reserv_lan[${K}]};
        fixed-address ${ip_reserv_lan[${K}]};
    }

EOF
    done
    printf "}\n\n" >> /etc/dhcp/dhcpd.conf
fi

# WIRELESS SETTINGS:
if [[ ${wireless} -eq 1 ]]; then
    dhcpd_string+=(
        ['IP_WLAN']="${ip[WLAN]}"
        ['NET_WLAN']="${net[WLAN]}"
        ['MASK_WLAN']="${mask[WLAN]}"
    )
    cat >> /etc/dhcp/dhcpd.conf << EOF

subnet NET_WLAN netmask MASK_WLAN {
    option routers IP_WLAN;
    range START_WLAN END_WLAN;

EOF
    if [ ${#ip_reserv_wlan[@]} -gt 0 -a ${#mac_reserv_wlan[@]} -gt 0 ]; then
        for K in "${!ip_reserv_wlan[@]}"; do
            let "m++"
            cat >> /etc/dhcp/dhcpd.conf << EOF
    # WLAN RESERVATION_${m}:
    host ${K} {
        hardware ethernet ${mac_reserv_wlan[${K}]};
        fixed-address ${ip_reserv_wlan[${K}]};
    }

EOF
        done
    fi
    printf "}\n\n" >> /etc/dhcp/dhcpd.conf
fi

sed_conf "$(declare -p dhcpd_string)" "$(declare -p dhcpd_config)" "${LOGFILE}"

# Set up DHCP Ranges for all subnets:
for i in "${!dhcp_range_START[@]}"; do
    v=`echo ${net[${i}]} | cut -d"." -f -3`
    sed -i "s/START_${i}/${v}.${dhcp_range_START[${i}]}/" /etc/dhcp/dhcpd.conf 2>> ${LOGFILE} 1> /dev/null
    sed -i "s/END_${i}/${v}.${dhcp_range_END[${i}]}/" /etc/dhcp/dhcpd.conf 2>> ${LOGFILE} 1> /dev/null
done
