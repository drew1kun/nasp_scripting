#!/usr/bin/env bash

print_status "SETTING UP WIRELESS ACCESS POINT"

print_status "Installing hostapd package"
yum install -y hostapd 2>> ${LOGFILE} 1> /dev/null
check_exit "hostapd package installed" "Failed installing hostapd package"

print_status "Enabling hostapd service to start on boot"
systemctl enable hostapd.service 2>> ${LOGFILE} 1> /dev/null
check_exit "hostapd service enabled" "hostapd service disabled"

print_status "Configuring hostapd"
check_dir 'src/etc/hostapd' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "hostapd service configured" "hostapd service configuration is incomplete"

declare -A hostapd_config=(
    ['CONFIG']="/etc/hostapd/hostapd.conf"
)

declare -A hostapd_string=(
    ['IF_WLAN']="${if[WLAN]}"
    ['ESSID_WLAN']="${essid_wlan}"
    ['WPA_PASSPHRASE']="${wpa_passphrase}"
)

sed_conf "$(declare -p hostapd_string)" "$(declare -p hostapd_config)" "${LOGFILE}"
