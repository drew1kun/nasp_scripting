#!/usr/bin/env bash

print_status "SETTING UP DYNAMIC ROUTING SERVICE"

print_status "Installing 'quagga' package"
yum install -y quagga 2>> ${LOGFILE} 1> /dev/null
check_exit "quagga package installed" "Failed installing quagga package"

print_status "Starting zebra service first time"
systemctl start zebra.service 2>> ${LOGFILE} 1> /dev/null
check_exit "zebra service started" "zebra service failed"

print_status "Enabling zebra service to start on boot"
systemctl enable zebra.service 2>> ${LOGFILE} 1> /dev/null
check_exit "zebra service enabled" "zebra service disabled"

print_status "Starting ospfd service first time"
systemctl start ospfd.service 2>> ${LOGFILE} 1> /dev/null
check_exit "ospfd service started" "ospfd service failed"

print_status "Enabling ospfd service to start on boot"
systemctl enable ospfd.service 2>> ${LOGFILE} 1> /dev/null
check_exit "ospfd service enabled" "ospfd service disabled"

print_status "Configuring Dynamic Routing"
print_status "Copying quagga configuration files"
check_dir 'src/etc/quagga' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "/etc/quagga directory copied" "/etc/quagga directory was not copied completely"
chown -R quagga:quagga /etc/quagga 2>> ${LOGFILE} 1> /dev/null
check_exit "fixed the /etc/quagga ownership" "/etc/quagga directory and files have wrong ownership"

# Change all interfaces configuration in zebra.conf and ospfd.conf in accordance with config.sh:
declare -A quagga_config=(
    ['ZEBRA']='/etc/quagga/zebra.conf'
    ['OSPF']='/etc/quagga/ospfd.conf'
)
declare -A quagga_string=()

for K in "${!if[@]}"; do
    quagga_string+=( ["IF_${K}"]="${if[${K}]}" )
    quagga_string+=( ["IP_${K}"]="${ip[${K}]}" )
    quagga_string+=( ["CIDR_${K}"]="${cidr[${K}]}" )
    quagga_string+=( ["NET_${K}"]="${net[${K}]}" )
done

sed_conf "$(declare -p quagga_string)" "$(declare -p quagga_config)" "${LOGFILE}"

if [[ ${wireless} -eq 0 ]]; then
    sed -i '/WLAN/,+1d' '/etc/quagga/ospfd.conf'
    sed -i '/IF_WLAN/,+4d' 'src/etc/quagga/zebra.conf'
fi

service_restart 'zebra'
service_restart 'ospfd'
