#!/usr/bin/env bash

print_status "SETTING UP FIREWALL RULES"

print_status "Installing iptables-services package"
yum install -y iptables-services 2>> ${LOGFILE} 1> /dev/null
check_exit "iptables-services package installed" "iptables-services package installation failed"

print_status "Starting iptables service first time"
systemctl start iptables.service 2>> ${LOGFILE} 1> /dev/null
check_exit "iptables service started" "iptables service failed"

print_status "Enabling iptables service to start on boot"
systemctl enable iptables.service 2>> ${LOGFILE} 1> /dev/null
check_exit "iptables service enabled" "iptables service disabled"

#---------------------------------------------------------------------------------
#Preliminary Setup
#---------------------------------------------------------------------------------
#REMOVE ALL RULES FROM PARTICULAR TABLE:
iptables -F -t filter  2>> ${LOGFILE} 1> /dev/null      # flush “filter” table
iptables -F -t nat 2>> ${LOGFILE} 1> /dev/null          # flush “nat” table
#Delete all user Defined Chains
iptables -X 2>> ${LOGFILE} 1> /dev/null

# CHECK POINT:
printf "\n" >> ${LOGFILE}
echo "iptables -S (before applying rules):" >> ${LOGFILE}
iptables -S >> ${LOGFILE} 2>&1

#---------------------------------------------------------------------------------
#Filter INPUT Rules (i.e. for datagrams comming into the router itself)
#---------------------------------------------------------------------------------
#POLICIES (i.e. the default)
# DENY ALL INCOMING TRAFFIC:
#iptables -t filter -P  INPUT DROP
iptables -t filter -P   INPUT ACCEPT 2>> ${LOGFILE} 1> /dev/null

# LOOPBACK
# ACCEPT ALL INCOMING COMMUNICATIONS FOR LOOPBACK INTERFACE:
iptables -t filter -I   INPUT   -i lo   -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

# ACCEPT ALL THE INCOMING DATA FOR ESTABLISHED AND RELATED CONNECTIONS
iptables -t filter -I   INPUT   -m state     --state ESTABLISHED,RELATED     -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

for i in ${!if[@]}
do
    # SSH
    # ACCEPT THE SSH CONNECTION TO NETWORK DEVICE FROM ANYWHERE:
    iptables -t filter -I   INPUT   -i ${if[${i}]}  -p tcp --dport 22   -m state    --state NEW   -j ACCEPT 2>> ${LOGFILE} 1> /dev/null
    #
    # DNS
    # ACCEPT ALL INCOMING TCP DNS REQUESTS ON THE NETWORK DEVICE FROM ANYWHERE:
    iptables -t filter -A   INPUT   -i ${if[${i}]}  -p tcp --dport 53 -m state      --state NEW -j ACCEPT 2>> ${LOGFILE} 1> /dev/null
    # ACCEPT ALL INCOMING UDP DNS REQUESTS ON THE NETWORK DEVICE FROM ANYWHERE:
    iptables -t filter -A   INPUT   -i ${if[${i}]}  -p udp --dport 53   -j ACCEPT 2>> ${LOGFILE} 1> /dev/null
done

for i in ${interfaces[@]}
do
    # ICMP
    # ACCEPT ICMP ECHO COMING TO THE NETWORK DEVICE FROM ANYWHERE EXCEPT WIFI NETWORK:
    iptables -t filter -I   INPUT   -i $i  -p icmp    -j ACCEPT 2>> ${LOGFILE} 1> /dev/null
done

#----ROUTING----
# OSPF
# ACCEPT OSPF TRAFFIC ON MULTICAST IP:
# In bigger environment we can use all zones multicast IPs:
iptables -t filter -A     INPUT   -i ${if['WAN']}     -d 224.0.0.2,224.0.0.4,224.0.0.5,224.0.0.6 -p 89 -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

# LOG ALL INPUT CONNECTIONS IN /var/log/messages (can be changed in iptables config):
iptables -t filter -A   INPUT   -j LOG 2>> ${LOGFILE} 1> /dev/null

# DROP EVERYTHING THAT DOES NOT MATCH THE RULES (NOT NEEDED WHEN DROP POLICY IS USED):
iptables -t filter -A   INPUT  -j DROP 2>> ${LOGFILE} 1> /dev/null

#---------------------------------------------------------------------------------
#Filter OUTPUT Rules (i.e. for datagrams leaving the router itself)
#---------------------------------------------------------------------------------
# POLICIES (i.e. the default)
# ACCEPT OUTGOING TRAFFIC:
iptables -t filter -P   OUTPUT ACCEPT 2>> ${LOGFILE} 1> /dev/null

#---------------------------------------------------------------------------------
#Filter FORWARD Rules (i.e. for datagrams being forwarded by the router)
#---------------------------------------------------------------------------------
# POLICIES (i.e. the default)
# DROP ALL FORWARDING PACKETS BY DEFAULT:
#iptables -t filter -P  FORWARD DROP
iptables -t filter -P   FORWARD ACCEPT 2>> ${LOGFILE} 1> /dev/null

# FORWARD ALL PACKETS ASSOCIATED WITH THE EXISTING CONNECTION
iptables -t filter -A   FORWARD -o ${if['LAN']} -m state --state ESTABLISHED,RELATED    -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

# FORWARD SSH CONNECTION FROM OUTSIDE WORLD (${if['WAN']}) TO MAIL SERVER:
iptables -t filter -A   FORWARD -i ${if['WAN']} -o ${if['LAN']} -d 10.16.19.1/25 -p tcp --dport ssh  -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

# FORWARD ALL TRAFFIC FROM WIRED STUDENT'S NETWORK TO OUTSIDE WORLD:
iptables -t filter -I   FORWARD -i ${if['LAN']} -o ${if['WAN']} -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

# FORWARD ALL ICMP REQUESTS (FOR COMMUNICATION TEST PURPOSES):
iptables -t filter -A   FORWARD -p icmp   -j ACCEPT 2>> ${LOGFILE} 1> /dev/null

if [[ ${wireless} -eq 1 ]]; then
    # FORWARD ALL TRAFFIC FROM WIFI NETWORK TO OUTSIDE WORLD:
    iptables -t filter -I   FORWARD -s ${net['WLAN']}/${cidr['WLAN']} -o ${if['WAN']} -j ACCEPT 2>> ${LOGFILE} 1> /dev/null
fi

# LOG ALL INPUT CONNECTIONS IN /var/log/messages (can be changed in iptables config):
iptables -t filter -A   FORWARD -j LOG 2>> ${LOGFILE} 1> /dev/null

# DROP EVERYTHING THAT DOES NOT MATCH THE RULES (NOT NEEDED WHEN DROP POLICY IS USED):
iptables -t filter -A   FORWARD -j DROP 2>> ${LOGFILE} 1> /dev/null

# CHECK POINT:
printf "\n" >> ${LOGFILE}
echo "iptables -S (after applying rules:)" >> ${LOGFILE}
iptables -S >> ${LOGFILE} 2>&1

# SAVE RULES:
iptables-save > /etc/sysconfig/iptables

service_restart 'iptables'
