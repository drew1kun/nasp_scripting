#!/usr/bin/env bash

print_status "SETTING UP SMTP SERVER USING POSTFIX"

print_status "Installing postfix package"
yum install -y postfix 2>> ${LOGFILE} 1> /dev/null
check_exit "postfix package installed" "Failed installing postfix package"

print_status "Enabling postfix service to start on boot"
systemctl enable postfix.service 2>> ${LOGFILE} 1> /dev/null
check_exit "postfix service enabled" "postfix service disabled"

print_status "Configuring postfix"
check_dir 'src/etc/postfix' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "postfix service configured" "postfix service configuration is incomplete"

declare -A postfix_config=(
    ['CONFIG']="/etc/postfix/main.cf"
)

declare -A postfix_string=(
    ['HOSTNAME']="${hostname}"
    ['DOMAIN']="${domain}"
)

sed_conf "$(declare -p postfix_string)" "$(declare -p postfix_config)" "${LOGFILE}"

service_restart 'postfix'
