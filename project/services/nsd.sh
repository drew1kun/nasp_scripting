#!/usr/bin/env bash

#----------------------NSD------------------------
print_status "SETTING UP AUTHORITATIVE DNS SERVER"
print_status "Installing 'nsd' package"
yum install -y nsd 2>> ${LOGFILE} 1> /dev/null
check_exit "nsd is installed" "Failed to install nsd"

print_status "Starting nsd service first time"
systemctl start nsd.service 2>> ${LOGFILE} 1> /dev/null
check_exit " service started" "nsd service failed"

print_status "Enabling nsd service to start on boot"
systemctl enable nsd.service 2>> ${LOGFILE} 1> /dev/null
check_exit "nsd service enabled" "nsd service disabled"

print_status "Configuring nsd"
print_status "Copying nsd configuration files"
check_dir 'src/etc/nsd' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "/etc/nsd directory copied" "/etc/nsd directory was not copied completely"

print_status "Renaming Zone files"
for K in "${!dns_zone[@]}"; do
    mv_file "/etc/nsd/${K}.zone" "/etc/nsd/${dns_zone[${K}]}.zone" 0 2>> ${LOGFILE} 1> /dev/null
    check_exit "New DNS Zone file: ${dns_zone[${K}]}.zone" "Failed renaming /etc/nsd/${K}.zone file"
done

declare -A nsd_config=(
    ['CONFIG']="/etc/nsd/nsd.conf"
    ['DOMAIN']="/etc/nsd/${domain}.zone"
    ['PTR_LAN']="/etc/nsd/${ptr_name}.zone"
)

declare -A nsd_string=(
    ['DOMAIN']="${domain}"
    ['IP_NSD']="${ip_nsd}"
    ['PTR_LAN']="${ptr_name}"
    ['HOSTNAME']="${hostname}"
)

sed_conf "$(declare -p nsd_string)" "$(declare -p nsd_config)" "${LOGFILE}"

# Modify A Records for individual hosts (mail, router, etc...):
for K in "${!srv[@]}"; do
    sed -i -r "/${K}..*[[:space:]]IN[[:space:]]A[[:space:]]/ s/IP_SRV/${srv[${K}]}/" /etc/nsd/${domain}.zone 2>> ${LOGFILE} 1> /dev/null
done

# Modify PTR Records for individual hosts (mail, router, etc...):
for K in "${!srv[@]}"; do
    ip_srv_host=`echo ${srv[$K]} | cut -d"." -f4`
    sed -i -r "/PTR[[:space:]]${K}./ s/IP_SRV_HOST/${ip_srv_host}/g" /etc/nsd/${ptr_name}.zone 2>> ${LOGFILE} 1> /dev/null
done

print_status "Checking nsd configuration files for syntax errors"

for K in "${!nsd_config[@]}"; do
    if [[ ${K} == 'CONFIG' ]]; then
        nsd-checkconf "${nsd_config[${K}]}" 2>> ${LOGFILE} 1> /dev/null
        check_exit "There are no errors in ${nsd_config[${K}]}" "There are some errors in ${nsd_config[${K}]}!"
    else
        nsd-checkzone "${dns_zone[${K}]}" "${nsd_config[${K}]}" 2>> ${LOGFILE} 1> /dev/null
        check_exit "There are no errors in ${nsd_config[${K}]}" "There are some errors in ${nsd_config[${K}]}!"
    fi
done

service_restart 'nsd'
