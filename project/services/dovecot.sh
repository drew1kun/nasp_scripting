#!/usr/bin/env bash

print_status "SETTING UP IMAP/POP3 SERVER USING DOVECOT"

print_status "Installing dovecot package"
yum install -y dovecot 2>> ${LOGFILE} 1> /dev/null
check_exit "dovecot package installed" "Failed installing dovecot package"

print_status "Enabling dovecot service to start on boot"
systemctl enable dovecot.service 2>> ${LOGFILE} 1> /dev/null
check_exit "dovecot service enabled" "dovecot service disabled"

print_status "Configuring dovecot"
check_dir 'src/etc/dovecot' 0 2>> ${LOGFILE} 1> /dev/null
check_dir 'src/etc/dovecot/conf.d' 0 2>> ${LOGFILE} 1> /dev/null
check_exit "dovecot service configured" "dovecot service configuration is incomplete"

service_restart 'dovecot'
