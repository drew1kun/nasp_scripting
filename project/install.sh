#!/usr/bin/env bash

source var.sh

check_root

case  $1  in
    'router')
    print_status "SETTING UP ROUTER"
    print_status "The Log file ${LOGFILE} was created. Check it for detailed info."
    declare mailserver=0
    source ./preconf.sh
    source ./services/network.sh

    ## DHCP
    if [[ ${dhcp} -eq 1 ]]; then
        source ./services/dhcpd.sh
    fi

    ## Routing:
    if [[ ${routing} -eq 1 ]]; then
        source ./services/quagga.sh
    fi

    ## DNS:
    if [[ ${dns} -eq 1 ]]; then
        source ./services/nsd.sh
        source ./services/unbound.sh
        sed -i -r "s/^DNS1=.*$/DNS1=127.0.0.1/" "/etc/sysconfig/network-scripts/ifcfg-${if[WAN]}"
    fi

    ## WIRELESS AP:
    if [[ ${wireless} -eq 1 ]]; then
        source ./services/hostapd.sh
    fi

    ## FIREWALL:
    if [[ ${firewall} -eq 1 ]]; then
        source ./services/iptables.sh
    fi
    final_msg ${ip_wan}
    ;;
    'mail')
    print_status "SETTING UP MAIL SERVER"
    print_status "The Log file ${LOGFILE} was created. Check it for detailed info."
    declare mailserver=1
    wireless=0
    hostname="mail.${domain}"
    source ./preconf.sh
    source ./services/network.sh
    source ./services/postfix.sh
    source ./services/dovecot.sh
    final_msg ${ip_mail}
    ;;
    '-h' | '--help')
    echo "This script will setup either the router box or mail server"
    echo "in accordance with specified arguments."
    echo "For example:"
    echo "  './main.sh router'"
    echo "OR:"
    echo "  './main.sh mail'"
    ;;
      *)
    print_error "please specify the setup option"
    echo "Use '-h' or '--help' options to see details"
    ;;
esac
