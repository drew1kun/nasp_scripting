#!/usr/bin/env bash

#-------------ROUTER SERVICES--------------
declare dhcp=1
declare dns=1
declare routing=1
declare wireless=1
declare firewall=1
#---------------------------------------
#---------SCRIPT CONFIGURATION----------

declare student_id='19'

# Domain Name:
declare domain="s${student_id}.as.learn"

# ROUTER Hostname:
declare hostname="s${student_id}rtr.as.learn"

# Network Interfaces:
declare if_wan='eth0'
declare if_lan='eth1'
declare if_wlan='wlan0'

# Mailserver IP address:
declare ip_mail="10.16.${student_id}.1"

declare gateway_wan='10.16.255.254'

# Specify network IPs:
declare net_wan="10.16.255.0"
declare net_lan="10.16.${student_id}.0"
declare net_wlan="10.16.${student_id}.128"

# Specify CIDR bits for each subnet:
declare cidr_wan=24
declare cidr_lan=25
declare cidr_wlan=25

# Specify ip address for each router's interface:
declare ip_wan="10.16.255.${student_id}"
declare ip_lan="10.16.${student_id}.126"
declare ip_wlan="10.16.${student_id}.254"
#---------------------------------------
#--------Wireless Configuration---------

declare essid_wlan="NASP_${student_id}"
declare wpa_passphrase='P@$$w0rd'
#---------------------------------------
#-------DNS Servers Configuration-------

# Authoritative dns server listening interface (WAN by default):
declare ip_nsd="${ip_wan}"
# Caching dns server listening interface (LAN by default):
declare ip_unbound="${ip_lan}"
# DNS Forwarder (for unbound):
# also used for initial internet access name resolution.
declare dns_forwarder='142.232.221.253'

# Specify DNS names of servers as ['Keys']
# and ip addresses of servers as "Values" in this array:
declare -A srv=(
    ['rtr']="${ip_lan}"
    ['mail']="${ip_mail}"
)
#---------------------------------------
#---------DHCP Configuration------------

# DHCP Ranges for all subnets:
dhcp_range_start_lan=100
dhcp_range_end_lan=125

dhcp_range_start_wlan=200
dhcp_range_end_wlan=253

# DHCP Reservations:
#
# LAN
# specify the host name as 'key'
# and MAC address as 'value':
declare -A mac_reserv_lan=(
    ['mail']="AA:BB:CC:DD:EE:FF"
)
# specify the host as 'key'
# and IP address as 'value':
declare -A ip_reserv_lan=(
    ['mail']="${srv['mail']}"
)

# WAN
declare -A mac_reserv_wlan=(

)
declare -A ip_reserv_wlan=(

)
#---------------------------------------
