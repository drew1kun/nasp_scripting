#!/usr/bin/env bash

print_status "PRECONFIGURING THE SYSTEM"
print_status "Changing hostname to ${hostname}"
hostnamectl set-hostname ${hostname}  2>> ${LOGFILE} 1> /dev/null
check_exit "Hostname is changed to ${hostname}" "Hostname was not changed"

print_status "Running netdev_bootstrap.sh:"
print_status "Setting up Instructor ssh key, EPEL repository, disabling SELINUX, firewalld, NetworkManager etc."
echo 'please wait...'
bash ./netdev_bootstrap.sh 2>> ${LOGFILE} 1> /dev/null
check_exit "netdev_bootstrap.sh script run successfully" "netdev_bootstrap.sh failed"
